@extends('layout.master')

@section('title')
  Edit Cast {{$cast->nama}}
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="title">Nama</label>
        <input type="text" value="{{$cast->nama}}" class="form-control" name="nama" id="title" placeholder="Masukkan Nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Umur</label>
        <input type="text" value="{{$cast->umur}}" class="form-control" name="umur" id="body" placeholder="Masukkan Umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Biodata</label>
        <textarea name="biodata" id="body" class="form-control" cols="30" rows="10">{{$cast->biodata}}</textarea>
        @error('biodata')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-success">Update</button>
</form>
@endsection